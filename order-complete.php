<?php

/**
 * This script is called, once a customer completes an order in our online shop.
 * It will send a thank-you email to the customer with an order-sumary.
 *
 * We only use the URL param `order_id` to identify the order in the database.
 *
 * -----------------------------------------------------------------------------
 *
 * Note, 13.06.2017 PSt:
 *  - This error is displayed very often in our logfiles, fix it!
 *    "Error in your SQL syntax; check the manual that corresponds to your MySQL
 *    server version for the right syntax to use near 'AND paid=1' at line 1"
 *  - Michael called: We need to send a copy of every email to "min@mirabit.com"!!
 *  - Apparently the price is not displayed in the email??
 *  - We can reuse a notification library from a different project. It will send
 *    the email and also report the order to our slack-chat-group.
 *    The libary is object oriented, we need to clean up this script so it can
 *    interact with that library... (notification-lib.php; it has some notes)
 *  - Also we *really* need to finally create a git repository for this script.
 *    So please create a fresh respository and commit all changes there.
 *
 * TODO: Complete as many tasks from above note as possible.
 *       Also feel free to clean up and improve the code, previous developers
 *       messed it up a bit ;)
 *
 *       !! We need the updated code until 14:00 !!
 *       (not all tasks needs to be done, but as much as possible)
 */

include 'config.php';
global $mysql;

$sql = "SELECT * FROM orders WHERE id=" . $_GET['order_id'] . " AND paid=1;";
$order = $mysql->query( $sql )->fetch_assoc();

$sql = "SELECT * FROM order_items WHERE order_id=" . $_GET['order_id'];
$order_items = $mysql->query( $sql )->fetch_all( MYSQLI_ASSOC );

$body = 'Du bist Klasse ' . $order['firstname']. '!<br><br>';
$body .= 'Wir haben deine Bestellung erhalten. Und wir freuen uns riesig darüber :)<br>';
$body .= 'Also erstmal ein ehrliches DANKE vom ganzen Team hier.<br><br>';
$body .= 'Wir sind total motiviert dran, deine Bestellung versandbereit zu machen. Die Post bringt dir spätestens in 2 Tagen die folgenden tollen Sachen:<br><br>';

foreach ( $order_items as $item ) {
	$body .= ' - ' . $item['amount'] . " x " . $item['name'] . ' (' .$item['prcie'].') ... <b>CHF ' . ($item['amount'] * $item['price']) . '</b><br>';
}

$body .= '<br>Wegen dir sind wir alle gut drauf, also werden wir doch am besten gleich Freunde auf Facebook: https://www.facebook.com/mirabit/, oder du schreibst uns einfach an, falls du eine Frage hast oder uns was Nettes sagen willst: support@mirabit.com<br><br>';
$body .= 'Bis bald - wir denken an dich.<br>Deine Freunde von MIRABIT ;)<br><br>';

// Prepare header details.
$email_to = 'philipp.stracker@mirabit.com';//$order['email'];
$subject = '🎉 Vielen Dank für deine Bestellung!';
$headers = "Content-Type: text/html; charset=UTF-8";

// Send the email!
mail(
	$email_to,
	$subject,
	$body,
	$headers
);

echo 'Email sent to ' . $email_to;
