<?php
/**
 * Notification API (Email and Slack)
 *
 * This demo API library takes an order-object and reports its details to a
 * predefined slack channel.
 *
 * To use this library you need to implement the interface IOrderItem into a
 * custom class and then pass the object of your custom class to the method
 * `report_order()` of the notification API object.
 *
 * Example:
 *
 * class MyOrder implements IOrderItem {
 *   ...
 *
 *   public function get_user_email() {
 *     return $this->order['email'];
 *   }
 *
 *   public function get_user_header( $headers ) {
 *     return $headers;
 *   }
 *
 *   ...
 * };
 *
 * $api = new NotificationApi( SLACK_HOOK, SLACK_CHANNEL );
 *
 * $order = new MyOrder( 123 );
 * $api->report( $order );
 */

/**
 * Describes the public properties of the param
 */
interface IOrderItem {
	/**
	 * Whether the order was paid already. This is the flag:
	 *     SELECT paid FROM orders
	 *
	 * @since  1.0.0
	 * @return bool The paid-flag of the order.
	 */
	public function was_paid();

	/**
	 * Return the order ID.
	 *
	 * @since  1.0.0
	 * @return int The order ID
	 */
	public function get_order_id();

	/**
	 * Return the customers email address.
	 *
	 * @since  1.0.0
	 * @return string The email address.
	 */
	public function get_user_email();

	/**
	 * Return the customers full name.
	 *
	 * @since  1.0.0
	 * @return string The full name "firstname lastname".
	 */
	public function get_user_name();

	/**
	 * Return the email subject. UTF-8 characters and emojis are supported!
	 *
	 * @since  1.0.0
	 * @return string The email subject.
	 */
	public function get_subject();

	/**
	 * Return the full email body (HTML/UTF-8 Format).
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public function get_email_body();

	/**
	 * Return additional email headers as array. Each array item is one header
	 * element. This is optional and can return an empty array.
	 *
	 * Note: Do not specify the content-type / charset header! It is already
	 *       predefined by the notification API.
	 *
	 * @since  1.0.0
	 * @param  array $headers The default headers. You can append new items to
	 *                        the array or simply return it unchanged.
	 * @return array
	 */
	public function get_email_headers( $headers );

	/**
	 * Return a list of all order items. Each list item must be an associative
	 * array with the below defined properties.
	 *
	 * @since  1.0.0
	 * @return array {
	 *                  'id' (int)
	 *                  'name' (string)
	 *                  'amount' (int)
	 *                  'price' (float)
	 *               }
	 */
	public function get_order_items();
}

/**
 * The notification API class
 *
 * To use this API you have to create a new instance and feed it the connection
 * details for slack in the constructor:
 *
 * $api = new NotificationApi( SLACK_HOOK, SLACK_CHANNEL );
 */
class NotificationApi {
	/**
	 * The slack API hook which is used to identify and communicate with your
	 * slack team.
	 *
	 * @var string
	 */
	private $slack_hook = false;

	/**
	 * The default notification target. It can be set to a channel or user ID.
	 *
	 * @var string
	 */
	private $slack_target = false;

	/**
	 * Initialize the Slack API.
	 *
	 * @since 1.0.0
	 * @param  string $slack_hook The public API hook, must be configured inside
	 *                            your slack app.
	 * @param  string $slack_target The channel or user who gets the notifications.
	 */
	public function __construct( $slack_hook, $slack_target ) {
		$this->slack_hook = $slack_hook;
		$this->slack_target = $slack_target;
	}

	/**
	 * Sends the email to the user and also reports the item to our slack
	 * channel.
	 *
	 * @since  1.0.0
	 * @param  IOrderItem $order The object to report.
	 */
	public function report( IOrderItem $order ) {
		if ( $order->was_paid() ) {
			$this->send_email( $order );
			$this->send_slack( $order );

			printf( 'Email sent to %s', $order->get_user_email() );
		} else {
			echo 'The order was not paid... Try a different order ID';
		}
	}

	/**
	 * Internal communication handler to send the email.
	 *
	 * @since  1.0.0
	 * @param  array $data Message details.
	 */
	protected function send_email( $order ) {
		// Convert the subject into valid UTF8 email-format.
		$subject = sprintf(
			'=?utf-8?B?%s?=',
			base64_encode( $order->get_subject() )
		);

		// Prepare nice recipient field.
		$recipient = sprintf(
			'%s <%s>',
			$order->get_user_name(),
			$order->get_user_email()
		);

		// Email body is already parsed and ready.
		$body = $order->get_email_body();

		// Email headers need to be converted from array to string.
		$headers = $order->get_email_headers(
			[ 'Content-Type: text/html; charset=UTF-8' ]
		);
		$headers = implode( "\r\n", $headers );

		mail( $recipient, $subject, $body, $headers );
	}

	/**
	 * Internal communication handler to send details to slack.
	 *
	 * @since  1.0.0
	 * @param  array $data Message details.
	 */
	protected function send_slack( $order ) {
		$slack_url = 'https://hooks.slack.com/services/' . $this->slack_hook;

		$order_items = [];
		$total_amount = 0;

		foreach ( $order->get_order_items() as $item ) {
			if ( empty( $item['amount'] ) ) { continue; }
			if ( empty( $item['price'] ) ) { continue; }

			$line_price = $item['amount'] * $item['price'];
			$total_amount += $line_price;

			$order_items[] = sprintf(
				'*%2$d* x *%1$s* á %3$s *%4$s*',
				$item['name'],
				$item['amount'],
				number_format( $item['price'], 2, ',', "'" ),
				number_format( $line_price, 2, ',', "'" )
			);
		}

		$message = [
			'fallback' => 'Bestellung wurde bezahlt!',
			'pretext' => sprintf(
				':tada: [DEMO] Bestellung *%d* wurde bezahlt!',
				$order->get_order_id()
			),
			'text' => implode( "\n", $order_items ),
			'fields' => [
				[
					'title' => 'Kunde',
					'value' => sprintf(
						"%s\n%s",
						$order->get_user_name(),
						$order->get_user_email()
					),
					'short' => true,
				],
				[
					'title' => 'Gesamtbetrag',
					'value' => 'CHF ' . number_format( $total_amount, 2, ',', "'" ),
					'short' => true,
				],
			],
			'mrkdwn_in' => [ 'text', 'pretext', 'fields' ],
		];

		$payload = [
			'attachments' => [ $message ],
			'mrkdwn' => true,
		];
		$payload['channel'] = $this->slack_target;

		// Correct encoding for the payload...
		$data = json_encode( $payload );

		// Send data to slack!
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $slack_url );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$repsonse = curl_exec( $ch );
		curl_close( $ch );
	}
};
