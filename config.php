<?php
/**
 * Database configuration for the online shop
 * bewerber.mirabit.com
 */
global $mysql;
error_reporting( 0 );

define( 'DB_NAME', 'db_w_bewerber' );
define( 'DB_HOST', 'localhost' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );

// Preparation for the NotificationApi integration (see notification-lib.php)
define( 'SLACK_HOOK', 'T03BRBKS6/B62NW8CBW/qy7iX0dC8fHHuva0kXuAQrpU' );
define( 'SLACK_CHANNEL', 'C62QGRCN9' ); #_mirabit-alerts

$mysql = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );

if ( $mysql->connect_error ) {
	die( 'Cannot connect to shop database: ' . $mysql->connect_error );
}
